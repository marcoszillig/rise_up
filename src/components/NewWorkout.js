import React, { useContext, useState } from "react";
import { WorkoutContext } from "../context/WorkoutContext";
import "../styles/NewWorkout.scss";

const NewWorkout = () => {
  const { dispatch } = useContext(WorkoutContext);
  const [exercise, setExercise] = useState("");
  const [sets, setSets] = useState("");
  const [reps, setReps] = useState("");
  const [weight, setWeight] = useState("");
  const handleSubmit = e => {
    e.preventDefault();
    dispatch({
      type: "ADD_WORKOUT",
      workout: {
        exercise,
        sets,
        reps,
        weight
      }
    });
    setExercise("");
    setSets("");
    setReps("");
    setWeight("");
    document.querySelector(".new-workout_form").classList.toggle("active");
  };

  const handleNewWorkout = () => {
    document.querySelector(".new-workout_form").classList.toggle("active");
    document.querySelector(".close-button").addEventListener("click", () => {
      document.querySelector(".new-workout_form").classList.remove("active");
    });
  };

  return (
    <div className="new-workout">
      <div className="add-button">
        <button onClick={handleNewWorkout}>adicionar</button>
      </div>
      <div className="new-workout_form">
        {/* <div className="overlay"></div> */}
        <div className="close-button">
          <button>X</button>
        </div>
        <form onSubmit={handleSubmit}>
          <label>Adicionar</label>
          <input
            type="text"
            placeholder="exercise"
            value={exercise}
            onChange={e => setExercise(e.target.value)}
            required
          />
          <input
            type="text"
            placeholder="set"
            value={sets}
            onChange={e => setSets(e.target.value)}
            required
          />
          <input
            type="text"
            placeholder="rep"
            value={reps}
            onChange={e => setReps(e.target.value)}
            required
          />
          <input
            type="text"
            placeholder="weight"
            value={weight}
            onChange={e => setWeight(e.target.value)}
            required
          />
          <input type="submit" value="add workout" />
        </form>
      </div>
    </div>
  );
};

export default NewWorkout;
