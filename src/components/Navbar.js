import React from "react";
import "../styles/Navbar.scss";

const Navbar = () => {
  const handleMenu = () => {
    document.querySelectorAll('.navbar-icon-lines')[1].classList.toggle('deactivated')
    document.querySelectorAll('.navbar-icon-lines')[0].classList.toggle('line-a')
    document.querySelectorAll('.navbar-icon-lines')[2].classList.toggle('line-b')
    document.querySelector('.navbar-links').classList.toggle('active')
  }

  return (
    <div className="navbar">
      <div className="navbar-icon" onClick={handleMenu}>
        <span className="navbar-icon-lines"></span>
        <span className="navbar-icon-lines" data-src="1"></span>
        <span className="navbar-icon-lines"></span>
      </div>
      <nav className="navbar-links" >
        <ul>
          <li>home</li>
          <li>workouts</li>
          <li>sobre</li>
          <li>contato</li>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
