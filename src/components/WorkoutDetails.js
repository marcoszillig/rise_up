import React from "react";
import "../styles/WorkoutDetails.scss";

const WorkoutDetails = ({ workout }) => {
  console.log('details', workout)
  return (
    <li className="workout-card">
      <h3 className="workout-card__title">{workout.exercise}</h3>
      <div className="workout-card__content">
        <div className="workout-card__content__sets detail">
          <span>{workout.sets}</span>
        </div>
        <div className="workout-card__content__reps detail">
          <span>{workout.reps}</span>
        </div>
        <div className="workout-card__content__weight detail">
          <span>{workout.weight}</span>
        </div>
        <div className="workout-card__content__type detail">
          <span>{workout.type}</span>
        </div>
      </div>
    </li>
  );
};

export default WorkoutDetails;
