import React, { useContext, useState, useEffect } from "react";
import { WorkoutContext } from "../context/WorkoutContext";
import WorkoutDetails from "./WorkoutDetails";
import '../styles/WorkoutList.scss'
import NewWorkout from "./NewWorkout";
import firebase from "../config/fbConfig"

const WorkList = () => {
  // const { workouts } = useContext(WorkoutContext);
  // console.log('list', workouts)
  const [workouts, setWorkouts] = useState([])
  useEffect(() => {
    firebase.firestore().collection('workouts').onSnapshot((snapshot) => {
      const workOuts = snapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
      }))
      setWorkouts(workOuts)
    })
  }, [])

  console.log(workouts)
  return (
    <div className="empty">No workouts. Free timeout.</div>
  )
  // workouts.length ? (
  //   <div className="workout-wrapper">
  //     <div className="workout-list--header">
  //       <h2>Workouts</h2>
  //       <NewWorkout />
  //     </div> 
  //     <div className="workout-list">
  //       <ul>
  //         {workouts.map(workout => {
  //           return <WorkoutDetails workout={workout} key={workout.id} />;
  //         })}
  //       </ul>
  //     </div>
  //   </div>
  // ) : 
  
};

export default WorkList;
