import React from "react";
import NewWorkout from "./NewWorkout";
import WorkoutContextProvider from "../context/WorkoutContext";
import "../styles/Sidebar.scss"

const Sidebar = (props) => {
  return (
    <div className="sidebar">  
      <div className="logo">
        <h1>riseUP</h1>
        <img src="" alt=""/>
      </div>
      <WorkoutContextProvider>
        <NewWorkout />    
      </WorkoutContextProvider>      
    </div>
  );
};

export default Sidebar;
