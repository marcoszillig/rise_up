import React from "react";
import WorkoutContextProvider from "../context/WorkoutContext";
import "../styles/Dashboard.scss";
import WorkList from "./WorkoutList";
import Sidebar from "./Sidebar";

const Dashboard = () => {  
  return (
    <div className="dashboard">
      <WorkoutContextProvider>
        <Sidebar/>
        <WorkList />
      </WorkoutContextProvider>
    </div>
  );
};

export default Dashboard;
