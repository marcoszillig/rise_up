import React, { createContext, useReducer, useState, useEffect } from 'react'
// import { workoutReducer } from '../Reducers/workoutReducer';
import firebase from "../config/fbConfig";
// import * as firebase from "firebase/app";

// import { useCollection } from 'react-firebase-hooks/firestore';

export const WorkoutContext = createContext();

const WorkoutContextProvider = (props) => {
  const [workouts, setWorkouts] = useState([])
  useEffect(() => {
   firebase
    .firestore()
    .collections('workouts')
    .onSnapshot(snapshot => {
      debugger
    })
  }, [])
  return (
    <WorkoutContext.Provider value={{ workouts }}>
      {props.children}
    </WorkoutContext.Provider>
  )
}

export default WorkoutContextProvider;