import React from "react";
import WorkoutContextProvider from "./context/WorkoutContext";
import Dashboard from "./components/Dashboard";
import Navbar from "./components/Navbar";
import NewWorkout from "./components/NewWorkout";
import WorkList from "./components/WorkoutList";

function App() {
  return (
    <div className="App">
      <Navbar />
      {/* <WorkoutContextProvider> */}
        {/* <Dashboard /> */}
        {/* <NewWorkout /> */}
        <WorkList />
      {/* </WorkoutContextProvider> */}
    </div>
  );
}

export default App;
